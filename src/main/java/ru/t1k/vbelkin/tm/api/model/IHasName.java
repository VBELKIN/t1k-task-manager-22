package ru.t1k.vbelkin.tm.api.model;

public interface IHasName {

    String getName();

    void setName();

}

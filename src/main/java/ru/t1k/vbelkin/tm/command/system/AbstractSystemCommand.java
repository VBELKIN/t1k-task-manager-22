package ru.t1k.vbelkin.tm.command.system;

import ru.t1k.vbelkin.tm.api.service.ICommandService;
import ru.t1k.vbelkin.tm.command.AbstractCommand;
import ru.t1k.vbelkin.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}

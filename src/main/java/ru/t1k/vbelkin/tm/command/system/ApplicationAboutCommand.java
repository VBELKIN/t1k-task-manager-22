package ru.t1k.vbelkin.tm.command.system;

public class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String NAME = "about";

    public static final String ARGUMENT = "-a";

    public static final String DESCRIPTION = "Display developer info.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[About]");
        System.out.println("Name: Vadim Belkin");
        System.out.println("E-mail: vbelkin@tsconsulting.ru");
        System.out.println("Web-Site: https://gitlab.com/VBELKIN");
    }

}
